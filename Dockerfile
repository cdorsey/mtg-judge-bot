FROM python:3.12-slim as base

ENV PYTHONUNBUFFERED=1

FROM base as builder

ARG POETRY_VERSION=1.8.2

ENV POETRY_NO_INTERACTION=1

WORKDIR /usr/src

RUN pip install poetry==${POETRY_VERSION}

COPY . .

RUN poetry build --format=wheel --no-cache

FROM base as runtime

ARG PUID=1000
ARG PGID=100

WORKDIR /usr/app

RUN groupadd -fg ${PGID} app && useradd -mu ${PUID} -g app app

COPY --from=builder --chown=app:app /usr/src/dist/*.whl /usr/src/rules.db .

USER app

RUN pip install *.whl

CMD ["python", "-m", "mtg_judge_bot.main"]
