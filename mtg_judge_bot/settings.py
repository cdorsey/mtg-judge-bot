from typing import ClassVar
from pydantic import SecretStr, Field
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    model_config: ClassVar[SettingsConfigDict] = {
        "case_sensitive": False,
        "env_file": ".env",
    }

    discord_token: SecretStr = Field(default="")
    openai_api_key: SecretStr = Field(default="")
    pinecone_api_key: SecretStr = Field(default="")
    anthropic_api_key: SecretStr = Field(default="")
