from contextvars import ContextVar
from sqlite3 import Cursor
from textwrap import dedent
from typing import Any, Type

import httpx
from langchain.agents import AgentExecutor, create_tool_calling_agent
from langchain_anthropic import ChatAnthropic
from langchain_core.messages import SystemMessage
from langchain_core.prompts.chat import (
    ChatPromptTemplate,
    HumanMessagePromptTemplate,
    MessagesPlaceholder,
)
from langchain_core.tools import BaseTool, ToolException
from langchain_openai import OpenAIEmbeddings
from langchain_pinecone import PineconeVectorStore
from pydantic.v1 import BaseModel, Field

from mtg_judge_bot.models import Card, Rulings
from mtg_judge_bot.settings import Settings

DB = ContextVar("db")


class CardTool(BaseTool):
    class Input(BaseModel):
        card_name: str = Field(
            ...,
            description="The name of the card to retrieve. Use exactly the same card name as the user, even if it's not the correct name.",
        )

    args_schema: Type[BaseModel] = Input

    async def _get_card(self, card_name: str) -> Card:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                "https://api.scryfall.com/cards/named", params={"fuzzy": card_name}
            )

        if response.status_code == 404:
            raise ToolException("This card does not exist")
        elif response.status_code != 200:
            raise ToolException("Unknown error occurred while fetching card.")

        return Card.model_validate(response.json())


class GetCardInfoTool(CardTool):
    name = "get_card_info"
    description = "Get information about a Magic: The Gathering card."

    def _run(self, card_name: str) -> str:
        return self._arun(card_name)

    async def _arun(self, card_name: str) -> dict[str, Any]:
        card = await self._get_card(card_name)

        return card.json(exclude=["rulings_uri"], exclude_none=True)


class GetCardRulingsTool(CardTool):
    name = "get_card_rulings"
    description = "Get the rulings for a Magic: The Gathering card."

    def _run(self, card_name: str) -> str:
        return self._arun(card_name)

    async def _arun(self, card_name: str) -> list[str]:
        card = await self._get_card(card_name)

        async with httpx.AsyncClient() as client:
            response = await client.get(str(card.rulings_uri))

        response.raise_for_status()

        rulings: Rulings = Rulings.model_validate(response.json())

        return [ruling.comment for ruling in rulings.data]


class GetRulesTextTool(BaseTool):
    class Input(BaseModel):
        topic: str = Field(
            ...,
            description="The topic to search for in the rulebook. (e.g. 'combat phase')",
        )

    name = "get_rules_text"
    description = "Get rulebook text relevant to a particular topic. Returns of up to 3 relevant rules."
    args_schema: Type[BaseModel] = Input

    def _get_rules_from_db(self, rule_ids: list[str]) -> list[str]:
        db = DB.get()
        placeholders = ", ".join("?" for _ in rule_ids)
        query = f"SELECT text FROM rules WHERE rule_id IN ({placeholders})"

        relevant_rules = db.execute(query, rule_ids).fetchall()
        relevant_rules = [text for (text,) in relevant_rules]
        return relevant_rules

    def _run(self, card_name: str) -> str:
        return self._arun(card_name)

    async def _arun(self, topic: str) -> list[str]:
        settings = Settings()

        embeddings = OpenAIEmbeddings(
            model="text-embedding-3-small",
            api_key=settings.openai_api_key.get_secret_value(),  # type: ignore
        )
        vector_store = PineconeVectorStore(
            index_name="magic-the-gathering",
            text_key="rule_id",
            embedding=embeddings,
            pinecone_api_key=settings.pinecone_api_key.get_secret_value(),
        )

        rule_vectors = await vector_store.asimilarity_search(
            topic, namespace="rules", k=3
        )

        return self._get_rules_from_db([rule.page_content for rule in rule_vectors])


async def judge(question: str, db: Cursor) -> str:
    INSTRUCTIONS = dedent("""
        You are an expert Magic: The Gathering judge. Your role is to answer questions about game rules and
        make rulings to resolve disputes that come up during games.

        Identify all cards, permanents, spells, abilities, and effects that are relevant to answering the question.

        Think through your answer in a step-by-step manner:
        - Cite the specific rules, cards, and interactions that are applicable
        - Explain your reasoning clearly
        - If the question cannot be answered definitively based on the information provided or available tools, say so

        Then, provide your final ruling. Give a definitive yes-or-no answer to the question if possible. 
        If relevant, clarify what happens next in the game as a result of your ruling.

        Do not discuss anything outside the scope of the question asked. Do not make strategic suggestions
        for either player.

        Always defer to the most recent official rules and card wordings. If a card's printed text
        contradicts the official rules, the card takes precedence.
    """)

    DB.set(db)

    llm = ChatAnthropic(
        model="claude-3-opus-20240229",
        temperature=0.2,
        api_key=Settings().anthropic_api_key.get_secret_value(),
    )

    prompt = ChatPromptTemplate.from_messages(
        [
            SystemMessage(content=INSTRUCTIONS),
            MessagesPlaceholder(variable_name="chat_history", optional=True),
            HumanMessagePromptTemplate.from_template(
                input_variables=["input"], template="{input}"
            ),
            MessagesPlaceholder(variable_name="agent_scratchpad"),
        ]
    )

    tools = [
        GetCardInfoTool(),
        GetCardRulingsTool(),
        GetRulesTextTool(),
    ]

    agent = create_tool_calling_agent(llm, tools, prompt)

    agent_executor = AgentExecutor(agent=agent, tools=tools, verbose=True)

    response = (await agent_executor.ainvoke({"input": question})).get(
        "output", "Recieved no response. Try again later."
    )

    db.close()

    return response
