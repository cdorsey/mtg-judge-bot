import inspect
import logging
import sqlite3
import sys

import discord
from discord import context
from loguru import logger

from mtg_judge_bot.commands import judge as handle_judge
from mtg_judge_bot.settings import Settings


class InterceptHandler(logging.Handler):
    def emit(self, record: logging.LogRecord) -> None:
        # Get corresponding Loguru level if it exists.
        level: str | int
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno

        # Find caller from where originated the logged message.
        frame, depth = inspect.currentframe(), 0
        while frame and (depth == 0 or frame.f_code.co_filename == logging.__file__):
            frame = frame.f_back
            depth += 1

        logger.opt(depth=depth, exception=record.exc_info).log(
            level, record.getMessage()
        )


logging.basicConfig(handlers=[InterceptHandler()], level=0, force=True)

logger.configure(
    handlers=[
        {"sink": sys.stdout, "level": "INFO", "backtrace": True, "diagnose": True}
    ]
)

bot = discord.Bot()
rules_db = sqlite3.connect("rules.db")


@bot.slash_command(name="judge")
async def judge(ctx: context.ApplicationContext, question: str):
    logger.info("Received question: {!r}", question)
    await ctx.defer()

    response = await handle_judge(question, rules_db.cursor())
    logger.info("Responding with: {!r}", response)

    await ctx.respond(f"> {question}\n\n{response}")


if __name__ == "__main__":
    bot.run(Settings().discord_token.get_secret_value())
