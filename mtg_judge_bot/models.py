from typing import Self

from pydantic import BaseModel, ConfigDict, HttpUrl


class Config(ConfigDict):
    extra = "ignore"


class Card(BaseModel):
    model_config = Config()

    name: str
    mana_cost: str | None = None
    oracle_text: str | None = None
    power: int | None = None
    toughness: int | None = None
    rulings_uri: HttpUrl | None = None
    card_faces: list[Self] | None = None


class Rulings(BaseModel):
    model_config = Config()

    class Data(BaseModel):
        comment: str

    data: list[Data]


if __name__ == "__main__":
    raw_data = {
        "name": "Blessed Hippogriff // Tyr's Blessing",
        "scryfall_uri": "https://scryfall.com/card/clb/11/blessed-hippogriff-tyrs-blessing?utm_source=api",
        "rulings_uri": "https://api.scryfall.com/cards/b4590e53-ca8d-4896-a8cf-6af1e4bc456f/rulings",
        "card_faces": [
            {
                "name": "Blessed Hippogriff",
                "mana_cost": "{3}{W}",
                "oracle_text": "Flying\nWhen Blessed Hippogriff enters the battlefield, you gain 2 life.",
                "power": 3,
                "toughness": 2,
            },
            {
                "name": "Tyr's Blessing",
                "mana_cost": "{W}",
                "oracle_text": "Enchant creature\nEnchanted creature gets +1/+1 and has flying.",
            },
        ]
    }

    card = Card(**raw_data)

    print(card.model_dump_json(exclude_none=True))
